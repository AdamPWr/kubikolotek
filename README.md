# Kubikolotek
## Repozytorium - zbiór rozwiązań

Kody źródłowe z przykładowych list wraz z oznaczonymi poprawnymi odpowiedziami.

Listy dostępne pod adresem http://tomasz.kubik.staff.iiar.pwr.wroc.pl/dydaktyka/JezykiProgramowania/index.html

TODO:
 - [ ] Przeniesienie rozwiązań z https://bitbucket.org/pwr_wroc_w4/jp3_kubikolotek/src
 - [ ] Weryfikacja tychże
 - [ ] Rozwiązanie zadań z 2018
    - [ ] 01
    - [ ] 02