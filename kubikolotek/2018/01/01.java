/**
* 1. Co można powiedzieć o poniższym kodzie
* 
* a) Jego kompilacja powiedzie się
* b) Jego kompilacja nie powiedzie się (nieobsłużony wyjątek java.lang.NumberFormatException)
* c) Jego kompilacja nie powiedzie się (zła konwersja z int do String)
* d) Jego kompilacja nie powiedzie się (złe przeciążenie metody m)           POPRAWNA
**/

public class A {
    void m(int a) {
    }

    int m(String s) {
        return Integer.parseInt(s);
    }

    String m(int a) {
        return Integer.toString(a);
    }
}

// Złe przeciążenie
