package kubikolotek;
/*
5. Niech klasy Ai B będą zdefiniowane w jednym pliku jak niżej. Co można o nich powiedzieć?
		a) Wystąpi błąd kompilacji w linii 1
		b) Wystąpi błąd kompilacji w linii 2
POPRAWNAc) Wystąpi błąd kompilacji w linii 3
		d) Po kompilacji i uruchomieniu metody mainklasy Ana ekranie pojawi się B.m()
		*/
public abstract class z5r2017 {
	z5r2017(){ m();}			//1
	public abstract void m();
	public static void main(String... args)
	{
		z5r2017 a = new z5r2017B(); //2
	}
}


class z5r2017B extends z5r2017{
	void m() {					//3
		System.out.println("B.m()");
	}
}